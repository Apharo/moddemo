package com.paomarllo.moddemo.web.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.paomarllo.moddemo.dto.ModDTORequest;
import com.paomarllo.moddemo.dto.ModDTOResponse;
import com.paomarllo.moddemo.web.api.ModApi;

@SpringBootTest
public class ModApiControllerTest {
	
	@Autowired
	private ModApi modApi;
	
	@Test
	public void getTest() {
		assertEquals(modApi.findModGet(7, 5, 12345), new ModDTOResponse(12339));
		assertEquals(modApi.findModGet(5, 0, 4), new ModDTOResponse(0));
		assertEquals(modApi.findModGet(10, 5, 15), new ModDTOResponse(15));
		assertEquals(modApi.findModGet(17, 8, 54321), new ModDTOResponse(54306));
		assertEquals(modApi.findModGet(499999993, 9, 1000000000), new ModDTOResponse(999999995));
		assertEquals(modApi.findModGet(10, 5, 187), new ModDTOResponse(185));
		assertEquals(modApi.findModGet(2, 0, 999999999), new ModDTOResponse(999999998));
	}
	
	@Test 
	public void postTest() {
		assertEquals(modApi.findModPost(new ModDTORequest(7, 5, 12345)), new ModDTOResponse(12339));
		assertEquals(modApi.findModPost(new ModDTORequest(5, 0, 4)), new ModDTOResponse(0));
		assertEquals(modApi.findModPost(new ModDTORequest(10, 5, 15)), new ModDTOResponse(15));
		assertEquals(modApi.findModPost(new ModDTORequest(17, 8, 54321)), new ModDTOResponse(54306));
		assertEquals(modApi.findModPost(new ModDTORequest(499999993, 9, 1000000000)), new ModDTOResponse(999999995));
		assertEquals(modApi.findModPost(new ModDTORequest(10, 5, 187)), new ModDTOResponse(185));
		assertEquals(modApi.findModPost(new ModDTORequest(2, 0, 999999999)), new ModDTOResponse(999999998));
		
	}
}
