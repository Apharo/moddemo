package com.paomarllo.moddemo;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.paomarllo.moddemo.web.api.ModApi;

@SpringBootTest
class DemoApplicationTests {
	
	@Autowired
	private ModApi modApi;
	
	@Test
	public void contextLoads() {
		assertThat(modApi).isNotNull();
	}

}
