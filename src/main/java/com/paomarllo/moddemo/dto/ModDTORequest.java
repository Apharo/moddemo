package com.paomarllo.moddemo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ModDTORequest {
	private Integer x;
	private Integer y;
	private Integer k;
}
