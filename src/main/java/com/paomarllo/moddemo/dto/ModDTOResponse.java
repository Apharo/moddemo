package com.paomarllo.moddemo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ModDTOResponse {
	private Integer value;
}
