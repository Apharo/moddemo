package com.paomarllo.moddemo.web.api;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.paomarllo.moddemo.dto.ModDTORequest;
import com.paomarllo.moddemo.dto.ModDTOResponse;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@RequestMapping("/v1")
@OpenAPIDefinition(info = @Info(title = "Mod API", version = "v1"))
public interface ModApi {

	@GetMapping(value = "/mod", 
			produces = { MediaType.APPLICATION_JSON_VALUE, "application/xml;charset=UTF-8", "application/xml" })
	public ModDTOResponse findModGet(@RequestParam int x, @RequestParam int y, @RequestParam int k);

	@PostMapping(value = "/mod",
			consumes = { MediaType.APPLICATION_JSON_VALUE, "application/xml;charset=UTF-8", "application/xml" }, 
			produces = { MediaType.APPLICATION_JSON_VALUE, "application/xml;charset=UTF-8", "application/xml" })
	public ModDTOResponse findModPost(@RequestBody ModDTORequest modDto);

}
