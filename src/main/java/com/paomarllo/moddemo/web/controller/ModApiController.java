package com.paomarllo.moddemo.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.paomarllo.moddemo.dto.ModDTORequest;
import com.paomarllo.moddemo.dto.ModDTOResponse;
import com.paomarllo.moddemo.service.ModService;
import com.paomarllo.moddemo.web.api.ModApi;

@RestController
public class ModApiController implements ModApi {

	@Autowired
	ModService modService;

	@Override
	public ModDTOResponse findModGet(int x, int y, int k) {
		return new ModDTOResponse(modService.calculateModNumber(x, y, k));
	}

	@Override
	public ModDTOResponse findModPost(ModDTORequest modDto) {
		return new ModDTOResponse(modService.calculateModNumber(modDto.getX(), modDto.getY(), modDto.getK()));
	}
	
}
