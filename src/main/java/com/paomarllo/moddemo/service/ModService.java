package com.paomarllo.moddemo.service;

import org.springframework.stereotype.Service;

@Service
public interface ModService {

	public Integer calculateModNumber(int n, int x, int y);
}
