package com.paomarllo.moddemo.service.impl;

import org.springframework.stereotype.Service;

import com.paomarllo.moddemo.service.ModService;

@Service
public class ModServiceImpl implements ModService {

	@Override
	public Integer calculateModNumber(int x, int y, int k) {
		// Possible value of K as K1
		if (k - k % x + y <= k) {
			return (k - k % x + y);
		}
		// Possible value of K as K2
		else {
			return (k - k % x - (x - y));
		}
	}

}
